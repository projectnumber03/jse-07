
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

public class ApplicationTest {
    public void projectCreateTest() {
        Map<String, String> m = new HashMap<>();
        m.put("1", "one");
        m.remove("one");
        m.values().forEach(System.out::println);
    }

    private String getTestData() {
        StringJoiner commands = new StringJoiner("\n");
        commands.add("project-1");
        commands.add("Description of project-1");
        commands.add("02.11.2015");
        commands.add("03.11.2015");
        commands.add("project-2");
        commands.add("Description of project-2");
        commands.add("04.11.2015");
        commands.add("05.11.2015");
        commands.add("all");
        commands.add("task-1");
        commands.add("Description of task-1");
        commands.add("02.11.2015");
        commands.add("03.11.2015");
        commands.add("task-2");
        commands.add("Description of task-2");
        commands.add("02.11.2015");
        commands.add("03.11.2015");
        commands.add("task-3");
        commands.add("Description of task-3");
        commands.add("02.11.2015");
        commands.add("03.11.2015");
        return commands.toString();
    }
}
