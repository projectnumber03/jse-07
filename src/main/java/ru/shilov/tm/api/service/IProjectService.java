package ru.shilov.tm.api.service;

import ru.shilov.tm.entity.Project;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.error.NumberToIdTransformException;

import java.util.List;

public interface IProjectService extends IService<Project> {

    List<Project> findByUserId(final String userId) throws NoSuchEntityException;

    Boolean removeByUserId(final String userId) throws EntityRemoveException;

    String getId(final String value, final String userId) throws NumberToIdTransformException;

}
