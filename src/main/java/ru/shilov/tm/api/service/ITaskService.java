package ru.shilov.tm.api.service;

import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.error.NumberToIdTransformException;

import java.util.List;

public interface ITaskService extends IService<Task> {

    List<Task> findByUserId(final String userId) throws NoSuchEntityException;

    Boolean removeByUserId(final String userId) throws EntityRemoveException;

    String getId(final String value, final String userId) throws NumberToIdTransformException;

}
