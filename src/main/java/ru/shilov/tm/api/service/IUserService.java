package ru.shilov.tm.api.service;

import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.error.NumberToIdTransformException;

import java.util.Optional;

public interface IUserService extends IService<User> {

    String getId(final String value) throws NumberToIdTransformException;

    Boolean containsLogin(final String login) throws NoSuchEntityException;

    Optional<User> findByLoginPasswd(final String login, final String passwd) throws NoSuchEntityException;

}
