package ru.shilov.tm.api.service;

public interface ITerminalService {

    String nextLine();

}
