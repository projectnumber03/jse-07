package ru.shilov.tm.api.service;

import ru.shilov.tm.error.EntityMergeException;
import ru.shilov.tm.error.EntityPersistException;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.repository.*;

import java.util.List;

public interface IService<T> {

    List<T> findAll();

    T findOne(final String id) throws NoSuchEntityException;

    void removeAll();

    Boolean removeOneByUserId(final String id, final String userId) throws EntityRemoveException;

    T persist(final T entity) throws EntityPersistException;

    T merge(final T entity) throws EntityMergeException;

    default Boolean isNullOrEmpty(final String id) {
        return id == null || id.isEmpty();
    }

    default boolean checkValue(final String value) {
        return !isNullOrEmpty(value) && value.matches("\\d+");
    }

}
