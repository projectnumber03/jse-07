package ru.shilov.tm.api.service;

import ru.shilov.tm.entity.User;

import java.util.List;
import java.util.Optional;

public interface IAuthorizationService {

    String getCurrentUserId();

    void setCurrentUser(final Optional<User> currentUser);

    Boolean hasAnyRole(final List<User.Role> roles);

}
