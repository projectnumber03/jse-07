package ru.shilov.tm.api.context;

import ru.shilov.tm.api.service.*;

public interface ServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    ITerminalService getTerminalService();

    IAuthorizationService getAuthorizationService();

}
