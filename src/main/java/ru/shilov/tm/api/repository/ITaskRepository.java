package ru.shilov.tm.api.repository;

import ru.shilov.tm.entity.Task;

public interface ITaskRepository extends IRepository<Task> {

    String getId(final String value, final String userId);

}
