package ru.shilov.tm.api.repository;

import ru.shilov.tm.entity.Project;

public interface IProjectRepository extends IRepository<Project> {

    String getId(final String value, final String userId);

}
