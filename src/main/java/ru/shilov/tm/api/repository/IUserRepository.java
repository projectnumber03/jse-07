package ru.shilov.tm.api.repository;

import ru.shilov.tm.entity.User;

import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    String getId(final String value);

    Boolean removeOneByUserId(final String id, final String userId);

    Optional<User> findByLoginPasswd(final String login, final String passwd);

    Boolean containsLogin(final String login);

}
