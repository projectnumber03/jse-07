package ru.shilov.tm.api.repository;

import java.util.List;

public interface IRepository<T> {

    List<T> findAll();

    T findOne(final String id);

    List<T> findByUserId(final String userId);

    void removeAll();

    Boolean removeByUserId(final String userId);

    Boolean removeOneByUserId(final String id, final String userId);

    T persist(final T entity);

    T merge(final T entity);

}
