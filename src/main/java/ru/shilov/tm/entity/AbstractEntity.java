package ru.shilov.tm.entity;


import java.util.UUID;

public abstract class AbstractEntity {

    protected String id = UUID.randomUUID().toString();

    protected AbstractEntity() {
    }

    protected AbstractEntity(final AbstractEntity entity) {
        this.id = entity.id;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public abstract String getUserId();

}
