package ru.shilov.tm.entity;

public final class User extends AbstractEntity {

    private String login;

    private String password;

    private Role role;

    public User() {
    }

    @Override
    public String getUserId() {
        return id;
    }

    public User(final String login, final String password, final Role role) {
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(final Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Пользователь: ").append(login).append("\n");
        sb.append("Роль: ").append(role.description);
        return sb.toString();
    }

    public enum Role {

        USER("Пользователь"),
        ADMIN("Администратор");

        Role(final String description) {
            this.description = description;
        }

        private final String description;

        public String getDescription() {
            return description;
        }

    }

}
