package ru.shilov.tm.entity;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public final class Project extends AbstractEntity {

    private String name;

    private String description;

    private LocalDate start;

    private LocalDate finish;

    private String userId;

    public Project() {
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Проект: ").append(this.name).append("\n");
        sb.append("Описание: ").append(this.description).append("\n");
        sb.append("Дата начала: ").append(DateTimeFormatter.ofPattern("dd.MM.yyyy").format(this.start)).append("\n");
        sb.append("Дата окончания: ").append(DateTimeFormatter.ofPattern("dd.MM.yyyy").format(this.finish));
        return sb.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public void setStart(final LocalDate start) {
        this.start = start;
    }

    public void setFinish(final LocalDate finish) {
        this.finish = finish;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

}
