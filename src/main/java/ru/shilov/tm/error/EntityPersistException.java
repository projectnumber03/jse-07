package ru.shilov.tm.error;

public final class EntityPersistException extends Exception {

    public EntityPersistException() {
        super("Ошибка добавления объекта");
    }

}
