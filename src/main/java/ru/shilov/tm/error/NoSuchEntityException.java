package ru.shilov.tm.error;

public final class NoSuchEntityException extends Exception {

    public NoSuchEntityException() {
        super("Ошибка получения объекта");
    }

}
