package ru.shilov.tm.error;

public final class DateTimeParseException extends Exception {

    public DateTimeParseException() {
        super("Некорректная дата");
    }

}
