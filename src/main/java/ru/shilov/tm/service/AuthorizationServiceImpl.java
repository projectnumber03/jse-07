package ru.shilov.tm.service;

import ru.shilov.tm.api.service.IAuthorizationService;
import ru.shilov.tm.entity.User;

import java.util.List;
import java.util.Optional;

public final class AuthorizationServiceImpl implements IAuthorizationService {

    private Optional<User> currentUser = Optional.empty();

    @Override
    public String getCurrentUserId() {
        return currentUser.get().getId();
    }

    @Override
    public void setCurrentUser(final Optional<User> currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    public Boolean hasAnyRole(final List<User.Role> roles) {
        return currentUser.isPresent() && roles.contains(currentUser.get().getRole());
    }

}
