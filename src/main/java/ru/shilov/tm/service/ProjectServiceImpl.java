package ru.shilov.tm.service;

import ru.shilov.tm.api.service.IProjectService;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.*;
import ru.shilov.tm.api.repository.IProjectRepository;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.api.repository.ITaskRepository;

import java.util.List;
import java.util.stream.Collectors;

public final class ProjectServiceImpl extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository repository;

    private final ITaskRepository taskRepo;

    public ProjectServiceImpl(IProjectRepository repository, ITaskRepository taskRepo) {
        this.repository = repository;
        this.taskRepo = taskRepo;
    }

    @Override
    public List<Project> findByUserId(final String userId) throws NoSuchEntityException {
        if (isNullOrEmpty(userId)) throw new NoSuchEntityException();
        return repository.findByUserId(userId);
    }

    @Override
    public Boolean removeByUserId(final String userId) throws EntityRemoveException {
        if (isNullOrEmpty(userId)) throw new EntityRemoveException();
        return repository.removeByUserId(userId);
    }

    @Override
    public Boolean removeOneByUserId(final String id, final String userId) throws EntityRemoveException {
        if (isNullOrEmpty(id) || isNullOrEmpty(userId)) throw new EntityRemoveException();
        taskRepo.findByUserId(userId).stream()
                .filter(t -> t.getProjectId().equals(id))
                .map(Task::getId)
                .collect(Collectors.toList())
                .forEach(taskId -> taskRepo.removeOneByUserId(taskId, userId));
        return repository.removeOneByUserId(id, userId);
    }

    @Override
    public String getId(final String value, final String userId) throws NumberToIdTransformException {
        if (!checkValue(value) || isNullOrEmpty(userId)) throw new NumberToIdTransformException(value);
        return repository.getId(value, userId);
    }

    @Override
    public IRepository<Project> getRepository() {
        return repository;
    }

}
