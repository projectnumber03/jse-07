package ru.shilov.tm.service;

import ru.shilov.tm.api.service.ITerminalService;

import java.util.Scanner;

public final class TerminalServiceImpl implements ITerminalService {

    private final Scanner scanner = new Scanner(System.in);

    @Override
    public String nextLine() {
        return scanner.nextLine();
    }

}
