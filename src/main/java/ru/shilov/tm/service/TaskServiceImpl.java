package ru.shilov.tm.service;

import ru.shilov.tm.api.service.ITaskService;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.*;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.api.repository.ITaskRepository;

import java.util.List;

public final class TaskServiceImpl extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository repository;

    public TaskServiceImpl(ITaskRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Task> findByUserId(final String userId) throws NoSuchEntityException {
        if (isNullOrEmpty(userId)) throw new NoSuchEntityException();
        return repository.findByUserId(userId);
    }

    @Override
    public Boolean removeByUserId(final String userId) throws EntityRemoveException {
        if (isNullOrEmpty(userId)) throw new EntityRemoveException();
        return repository.removeByUserId(userId);
    }

    @Override
    public Boolean removeOneByUserId(final String id, final String userId) throws EntityRemoveException {
        if (isNullOrEmpty(id) || isNullOrEmpty(userId)) throw new EntityRemoveException();
        return repository.removeOneByUserId(id, userId);
    }

    @Override
    public String getId(final String value, final String userId) throws NumberToIdTransformException {
        if (!checkValue(value) && isNullOrEmpty(userId)) throw new NumberToIdTransformException(value);
        return repository.getId(value, userId);
    }

    @Override
    public IRepository<Task> getRepository() {
        return repository;
    }

}
