package ru.shilov.tm.service;

import org.apache.commons.codec.digest.DigestUtils;
import ru.shilov.tm.api.service.IUserService;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.*;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.api.repository.IUserRepository;

import java.util.Optional;


public final class UserServiceImpl extends AbstractService<User> implements IUserService {

    private final IUserRepository repository;

    public UserServiceImpl(IUserRepository repository) {
        this.repository = repository;
    }

    @Override
    public Boolean removeOneByUserId(final String id, final String userId) throws EntityRemoveException {
        if (isNullOrEmpty(id) || isNullOrEmpty(userId)) throw new EntityRemoveException();
        return repository.removeOneByUserId(id, userId);
    }

    @Override
    public User persist(final User user) throws EntityPersistException {
        if (user == null) throw new EntityPersistException();
        user.setPassword(DigestUtils.md5Hex(user.getPassword()));
        return repository.persist(user);
    }

    @Override
    public String getId(final String value) throws NumberToIdTransformException {
        if (!checkValue(value)) throw new NumberToIdTransformException(value);
        return repository.getId(value);
    }

    @Override
    public Boolean containsLogin(final String login) throws NoSuchEntityException {
        if (isNullOrEmpty(login)) throw new NoSuchEntityException();
        return repository.containsLogin(login);
    }

    @Override
    public Optional<User> findByLoginPasswd(String login, String passwd) throws NoSuchEntityException {
        if (isNullOrEmpty(login) || isNullOrEmpty(passwd)) throw new NoSuchEntityException();
        return repository.findByLoginPasswd(login, passwd);
    }

    @Override
    public IRepository<User> getRepository() {
        return repository;
    }

}
