package ru.shilov.tm.service;

import ru.shilov.tm.api.service.IService;
import ru.shilov.tm.entity.AbstractEntity;
import ru.shilov.tm.error.EntityMergeException;
import ru.shilov.tm.error.EntityPersistException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.api.repository.IRepository;

import java.util.List;

public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @Override
    public List<T> findAll() {
        return getRepository().findAll();
    }

    @Override
    public T findOne(final String id) throws NoSuchEntityException {
        final T entity = getRepository().findOne(id);
        if (isNullOrEmpty(id) || entity == null) {
            throw new NoSuchEntityException();
        }
        return entity;
    }

    @Override
    public void removeAll() {
        getRepository().removeAll();
    }

    @Override
    public T persist(final T entity) throws EntityPersistException {
        if (entity == null) throw new EntityPersistException();
        return getRepository().persist(entity);
    }

    @Override
    public T merge(final T entity) throws EntityMergeException {
        if (entity == null) throw new EntityMergeException();
        return getRepository().merge(entity);
    }

    abstract IRepository<T> getRepository();

}
