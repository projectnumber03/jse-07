package ru.shilov.tm.command.other;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class HelpCommand extends AbstractTerminalCommand {

    private final List<AbstractTerminalCommand> commands;

    public HelpCommand(List<AbstractTerminalCommand> commands) {
        this.commands = commands;
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Список доступных комманд";
    }

    @Override
    public void execute() {
        String helpMessage = commands.stream()
                .sorted(Comparator.comparing(AbstractTerminalCommand::getName))
                .map(tc -> String.format("%s: %s", tc.getName(), tc.getDescription()))
                .collect(Collectors.joining("\n"));
        System.out.println(helpMessage);
    }

    @Override
    public List<User.Role> getRoles() {
        return Collections.emptyList();
    }

}
