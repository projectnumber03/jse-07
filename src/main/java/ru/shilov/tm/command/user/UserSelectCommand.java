package ru.shilov.tm.command.user;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public final class UserSelectCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println("ВВЕДИТЕ ID ПОЛЬЗОВАТЕЛЯ:");
        final String userId = serviceLocator.getUserService().getId(serviceLocator.getTerminalService().nextLine());
        final User u = serviceLocator.getUserService().findOne(userId);
        System.out.println(u.toString());
        System.out.println("[OK]");
    }

    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN);
    }

    @Override
    public String getName() {
        return "user-select";
    }

    @Override
    public String getDescription() {
        return "Свойства пользователя";
    }

}
