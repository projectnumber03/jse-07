package ru.shilov.tm.command.user;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class UserFindAllCommand extends AbstractTerminalCommand {

    @Override
    public void execute() {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println(getAllUsers());
        System.out.println("[OK]");
    }

    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN);
    }

    @Override
    public String getName() {
        return "user-list";
    }

    @Override
    public String getDescription() {
        return "Список пользователей";
    }

    private String getAllUsers() {
        final List<User> users = serviceLocator.getUserService().findAll();
        return IntStream.range(1, users.size() + 1).boxed()
                .map(i -> (String.format("%d. %s", i, users.get(i - 1).getLogin())))
                .collect(Collectors.joining("\n"));
    }

}
