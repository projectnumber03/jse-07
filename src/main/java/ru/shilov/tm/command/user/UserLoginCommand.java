package ru.shilov.tm.command.user;

import org.apache.commons.codec.digest.DigestUtils;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.NoSuchEntityException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public final class UserLoginCommand extends AbstractTerminalCommand {

    @Override
    public String getName() {
        return "login";
    }

    @Override
    public String getDescription() {
        return "Авторизация пользователя";
    }

    @Override
    public void execute() throws NoSuchEntityException {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println("ВВЕДИТЕ ЛОГИН:");
        String login = serviceLocator.getTerminalService().nextLine();
        while (!serviceLocator.getUserService().containsLogin(login)) {
            System.out.println("[ПОЛЬЗОВАТЕЛЬ НЕ НАЙДЕН]");
            System.out.println("ВВЕДИТЕ ЛОГИН:");
            login = serviceLocator.getTerminalService().nextLine();
        }
        System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
        String passwd = serviceLocator.getTerminalService().nextLine();
        Optional<User> user;
        while (!(user = serviceLocator.getUserService().findByLoginPasswd(login, DigestUtils.md5Hex(passwd))).isPresent()) {
            System.out.println("[НЕВЕРНЫЙ ПАРОЛЬ]");
            System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
            passwd = serviceLocator.getTerminalService().nextLine();
        }
        serviceLocator.getAuthorizationService().setCurrentUser(user);
        System.out.println("[OK]");
    }

    @Override
    public List<User.Role> getRoles() {
        return Collections.emptyList();
    }

}
