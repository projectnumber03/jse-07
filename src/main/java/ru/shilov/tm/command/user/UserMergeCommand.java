package ru.shilov.tm.command.user;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public final class UserMergeCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println("ВВЕДИТЕ ID ПОЛЬЗОВАТЕЛЯ:");
        final String userId = serviceLocator.getUserService().getId(serviceLocator.getTerminalService().nextLine());
        final User u = serviceLocator.getUserService().findOne(userId);
        System.out.println("ВВЕДИТЕ ЛОГИН:");
        u.setLogin(serviceLocator.getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
        u.setPassword(serviceLocator.getTerminalService().nextLine());
        Arrays.asList(User.Role.values()).forEach(r -> System.out.println(String.format("%d. %s", r.ordinal() + 1, r.getDescription())));
        System.out.println("ВЫБЕРИТЕ РОЛЬ:");
        String roleId = serviceLocator.getTerminalService().nextLine();
        while (!roleCheck(roleId)) {
            System.out.println("ВЫБЕРИТЕ РОЛЬ:");
            roleId = serviceLocator.getTerminalService().nextLine();
        }
        u.setRole(User.Role.values()[Integer.parseInt(roleId) - 1]);
        serviceLocator.getUserService().merge(u);
        System.out.println("[OK]");
    }

    @Override
    public String getName() {
        return "user-edit";
    }

    @Override
    public String getDescription() {
        return "Редактирование пользователя";
    }

    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN);
    }

    private boolean roleCheck(final String roleId) {
        return roleId != null && !roleId.isEmpty()
                && roleId.matches("\\d+")
                && Integer.parseInt(roleId) <= User.Role.values().length;
    }

}
