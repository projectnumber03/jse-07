package ru.shilov.tm.command.project;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Project;
import ru.shilov.tm.entity.User;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;

public final class ProjectMergeCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        final String userId = serviceLocator.getAuthorizationService().getCurrentUserId();
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        final String projectId = serviceLocator.getProjectService().getId(serviceLocator.getTerminalService().nextLine(), userId);
        final Project p = serviceLocator.getProjectService().findOne(projectId);
        System.out.println("ВВЕДИТЕ НАЗВАНИЕ ПРОЕКТА:");
        p.setName(serviceLocator.getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ОПИСАНИЕ:");
        p.setDescription(serviceLocator.getTerminalService().nextLine());
        try {
            System.out.println("ВВЕДИТЕ ДАТУ НАЧАЛА:");
            p.setStart(LocalDate.parse(serviceLocator.getTerminalService().nextLine(), DateTimeFormatter.ofPattern("dd.MM.yyyy")));
            System.out.println("ВВЕДИТЕ ДАТУ ОКОНЧАНИЯ:");
            p.setFinish(LocalDate.parse(serviceLocator.getTerminalService().nextLine(), DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        } catch (DateTimeParseException e) {
            throw new ru.shilov.tm.error.DateTimeParseException();
        }
        serviceLocator.getProjectService().merge(p);
        System.out.println("[OK]");
    }

    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @Override
    public String getName() {
        return "project-edit";
    }

    @Override
    public String getDescription() {
        return "Редактирование проекта";
    }

}
