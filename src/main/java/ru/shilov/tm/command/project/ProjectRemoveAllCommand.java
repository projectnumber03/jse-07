package ru.shilov.tm.command.project;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public final class ProjectRemoveAllCommand extends AbstractTerminalCommand {

    @Override
    public void execute() {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        serviceLocator.getProjectService().removeAll();
        System.out.println("[ВСЕ ПРОЕКТЫ УДАЛЕНЫ]");
    }

    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN);
    }

    @Override
    public String getName() {
        return "project-remove-all";
    }

    @Override
    public String getDescription() {
        return "Полное удаление всех проектов";
    }

}
