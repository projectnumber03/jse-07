package ru.shilov.tm.command.task;

import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public final class TaskAttachCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        final String userId = serviceLocator.getAuthorizationService().getCurrentUserId();
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
        final String taskId = serviceLocator.getTaskService().getId(serviceLocator.getTerminalService().nextLine(), userId);
        final Task t = serviceLocator.getTaskService().findOne(taskId);
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        t.setProjectId(serviceLocator.getProjectService().getId(serviceLocator.getTerminalService().nextLine(), userId));
        System.out.println("[OK]");
    }

    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @Override
    public String getName() {
        return "task-attach";
    }

    @Override
    public String getDescription() {
        return "Добавление задачи в проект";
    }

}
