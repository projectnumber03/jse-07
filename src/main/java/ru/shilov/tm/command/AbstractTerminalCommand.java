package ru.shilov.tm.command;

import ru.shilov.tm.api.context.ServiceLocator;
import ru.shilov.tm.entity.User;

import java.util.List;

public abstract class AbstractTerminalCommand {

    protected ServiceLocator serviceLocator;

    public abstract void execute() throws Exception;

    public abstract List<User.Role> getRoles();

    public abstract String getName();

    public abstract String getDescription();

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
