package ru.shilov.tm.repository;

import ru.shilov.tm.api.repository.ITaskRepository;
import ru.shilov.tm.entity.Task;

import java.util.*;

public final class TaskRepositoryImpl extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public String getId(final String value, final String userId) {
        final List<Task> entities = findByUserId(userId);
        return entities.size() >= Integer.parseInt(value) ? entities.get(Integer.parseInt(value) - 1).getId() : "";
    }

}
