package ru.shilov.tm.repository;

import ru.shilov.tm.api.repository.IProjectRepository;
import ru.shilov.tm.entity.Project;

import java.util.*;

public final class ProjectRepositoryImpl extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public String getId(final String value, final String userId) {
        final List<Project> projects = findByUserId(userId);
        return projects.size() >= Integer.parseInt(value) ? projects.get(Integer.parseInt(value) - 1).getId() : "";
    }

}
