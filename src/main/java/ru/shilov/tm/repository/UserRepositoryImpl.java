package ru.shilov.tm.repository;

import ru.shilov.tm.api.repository.IUserRepository;
import ru.shilov.tm.entity.User;

import java.util.*;

public final class UserRepositoryImpl extends AbstractRepository<User> implements IUserRepository {

    @Override
    public Boolean removeOneByUserId(final String id, final String userId) {
        return entities.entrySet().removeIf(entry -> entry.getValue().getId().equals(id) && !entry.getValue().getId().equals(userId));
    }

    @Override
    public Optional<User> findByLoginPasswd(String login, String passwd) {
        return entities.values().stream().filter(u -> u.getLogin().equals(login) && u.getPassword().equals(passwd)).findAny();
    }

    @Override
    public Boolean containsLogin(String login) {
        return entities.values().stream().anyMatch(u -> u.getLogin().equals(login));
    }

    @Override
    public String getId(final String value) {
        return entities.size() >= Integer.parseInt(value) ? new ArrayList<>(entities.values()).get(Integer.parseInt(value) - 1).getId() : "";
    }

}
