package ru.shilov.tm.repository;

import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    protected final Map<String, T> entities = new LinkedHashMap<>();

    @Override
    public List<T> findAll() {
        return new ArrayList<>(entities.values());
    }

    @Override
    public T findOne(final String id) {
        return entities.values().stream().filter(entity -> entity.getId().equals(id)).findAny().orElse(null);
    }

    @Override
    public List<T> findByUserId(final String userId) {
        return entities.values().stream().filter(entity -> entity.getUserId().equals(userId)).collect(Collectors.toList());
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    public Boolean removeByUserId(final String userId) {
        return entities.entrySet().removeIf(entry -> entry.getValue().getUserId().equals(userId));
    }

    @Override
    public Boolean removeOneByUserId(final String id, final String userId) {
        return entities.entrySet().removeIf(entry -> entry.getValue().getId().equals(id) && entry.getValue().getUserId().equals(userId));
    }

    @Override
    public T persist(final T entity) {
        return entities.put(entity.getId(), entity);
    }

    @Override
    public T merge(final T entity) {
        return entities.merge(entity.getId(), entity, (oldEntity, newEntity) -> newEntity);
    }

}
