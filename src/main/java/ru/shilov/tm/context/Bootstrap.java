package ru.shilov.tm.context;

import ru.shilov.tm.api.context.ServiceLocator;
import ru.shilov.tm.api.repository.IProjectRepository;
import ru.shilov.tm.api.repository.ITaskRepository;
import ru.shilov.tm.api.repository.IUserRepository;
import ru.shilov.tm.api.service.*;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.command.other.AboutCommand;
import ru.shilov.tm.command.other.ExitCommand;
import ru.shilov.tm.command.other.HelpCommand;
import ru.shilov.tm.command.project.*;
import ru.shilov.tm.command.task.*;
import ru.shilov.tm.command.user.*;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.EntityPersistException;
import ru.shilov.tm.error.IllegalCommandException;
import ru.shilov.tm.error.InitializationException;
import ru.shilov.tm.error.PermissionException;
import ru.shilov.tm.repository.*;
import ru.shilov.tm.service.*;

import java.util.*;
import java.util.stream.Collectors;

public final class Bootstrap implements ServiceLocator {

    private final IProjectRepository projectRepo = new ProjectRepositoryImpl();

    private final ITaskRepository taskRepo = new TaskRepositoryImpl();

    private final IUserRepository userRepo = new UserRepositoryImpl();

    private final ITerminalService terminalService = new TerminalServiceImpl();

    private final IAuthorizationService authorizationService = new AuthorizationServiceImpl();

    private final IProjectService projectService = new ProjectServiceImpl(projectRepo, taskRepo);

    private final ITaskService taskService = new TaskServiceImpl(taskRepo);

    private final IUserService userService = new UserServiceImpl(userRepo);

    private final Map<String, AbstractTerminalCommand> commands = initCommands();

    public void init() throws Exception {
        System.out.println("*** ДОБРО ПОЖАЛОВАТЬ В ПЛАНИРОВЩИК ЗАДАЧ ***");
        initUsers();
        while (true) {
            try {
                final String commandName = terminalService.nextLine();
                if (!commands.containsKey(commandName)) throw new IllegalCommandException();
                final AbstractTerminalCommand command = commands.get(commandName);
                if (!command.getRoles().isEmpty() && !authorizationService.hasAnyRole(command.getRoles())) {
                    throw new PermissionException();
                }
                command.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Map<String, AbstractTerminalCommand> initCommands() {
        final List<AbstractTerminalCommand> commands = new ArrayList<>(Arrays.asList(
                new TaskFindAllCommand()
                , new ProjectFindAllCommand()
                , new ProjectFindOneCommand()
                , new TaskMergeCommand()
                , new ProjectMergeCommand()
                , new TaskPersistCommand()
                , new ProjectPersistCommand()
                , new TaskRemoveAllCommand()
                , new ProjectRemoveAllCommand()
                , new TaskRemoveCommand()
                , new ProjectRemoveCommand()
                , new TaskSelectCommand()
                , new ProjectSelectCommand()
                , new TaskAttachCommand()
                , new ExitCommand()
                , new UserFindAllCommand()
                , new UserLoginCommand()
                , new UserLogoutCommand()
                , new UserMergeCommand()
                , new UserPasswordChangeCommand()
                , new UserRegisterCommand()
                , new UserSelectCommand()
                , new ProjectClearCommand()
                , new TaskClearCommand()
                , new AboutCommand()
        ));
        commands.add(new HelpCommand(commands));
        commands.forEach(tc -> tc.setServiceLocator(this));
        return commands.stream().collect(Collectors.toMap(tc -> Objects.requireNonNull(tc).getName(), tc -> tc));
    }

    private void initUsers() throws Exception {
        try {
            userService.persist(new User("user", "123", User.Role.USER));
            userService.persist(new User("admin", "123", User.Role.ADMIN));
        } catch (EntityPersistException e) {
            throw new InitializationException();
        }
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IUserService getUserService() {
        return userService;
    }

    public ITerminalService getTerminalService() {
        return terminalService;
    }

    public IAuthorizationService getAuthorizationService() {
        return authorizationService;
    }

}
