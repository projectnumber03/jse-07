package ru.shilov.tm;

import ru.shilov.tm.context.Bootstrap;

public final class Application {

    public static void main(final String[] args) throws Exception {
        new Bootstrap().init();
    }

}
